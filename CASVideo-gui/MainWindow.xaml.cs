﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;

using CAS;
using ControlzEx.Theming;
using Ookii.Dialogs.Wpf;
using MahApps.Metro.Controls;

using Utils = CAS.Utils;

namespace CASVideo_gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public string OutputDirectory { get; set; }
        public string WorkingDirectory { get; } = Path.Combine(Environment.CurrentDirectory, "Working");
        private readonly bool loading = true;
        private bool inputVideoPaused = true;
        private bool outputVideoPaused = true;
        private ContentAwareScaleVideo cas;
        public string[] VideoFormats { get; } = { ".avi", ".flv", ".mkv", ".mov", ".mp4", ".webm", ".wmv", };
        private CancellationTokenSource cancellationTokenSource = new();
        private CancellationToken processingCancellationToken;
        private Task processingTask;
        public MainWindow()
        {
            InitializeComponent();
            // Applying Theme
            _ = ThemeManager.Current.ChangeTheme(this, Properties.Settings.Default.Theme);
            string[] ThemeParts = Properties.Settings.Default.Theme.Split('.');
            ThemeDark.IsChecked = ThemeParts[0] == "Dark";
            ThemeAccent.ItemsSource = Accents;
            ThemeAccent.SelectedIndex = Array.IndexOf(Accents, ThemeParts[1]);
            DotNetVersion.Text = $"Powered by {RuntimeInformation.FrameworkDescription}";
            LoadSettings();
            loading = false;
            if (Directory.Exists("Working"))
            {
                Directory.Delete("Working", true);
            }
            CreatedBy.Text = $"CASVideo-gui Version {System.Reflection.Assembly.GetEntryAssembly().GetName().Version} created by Taconator";
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                string filePath = args[1];
                string filePathLower = filePath.ToLowerInvariant();
                if (VideoFormats.Any(filetype => filePathLower.EndsWith(filetype)))
                {
                    InputVideo.Text = filePath;
                    ProcessButton_Click(null, null);
                }
                else
                {
                    _ = MessageBox.Show($"Input file must one of these file types: {string.Join(',', VideoFormats)}", "Cannot use dropped file", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            processingCancellationToken = cancellationTokenSource.Token;
        }

        private string[] Accents { get; } =
        {
            "Red",
            "Green",
            "Blue",
            "Purple",
            "Orange",
            "Lime",
            "Emerald",
            "Teal",
            "Cyan",
            "Cobalt",
            "Indigo",
            "Violet",
            "Pink",
            "Magenta",
            "Crimson",
            "Amber",
            "Yellow",
            "Brown",
            "Olive",
            "Steel",
            "Mauve",
            "Taupe",
            "Sienna"
        };

        private void LoadSettings()
        {
            _ = ThemeManager.Current.ChangeTheme(this, Properties.Settings.Default.Theme);
            string[] ThemeParts = Properties.Settings.Default.Theme.Split('.');
            ThemeDark.IsChecked = ThemeParts[0] == "Dark";
            ThemeAccent.ItemsSource = Accents;
            ThemeAccent.SelectedIndex = Array.IndexOf(Accents, ThemeParts[1]);
            if (string.IsNullOrEmpty(Properties.Settings.Default.OutputDirectory))
            {
                OutputDirectory = Path.Combine(Environment.CurrentDirectory, "Output");
                _ = Directory.CreateDirectory(OutputDirectory);
                Properties.Settings.Default.OutputDirectory = OutputDirectory;
                Properties.Settings.Default.Save();
            }
            OutputDirectoryBox.Text = Properties.Settings.Default.OutputDirectory;
            ScaleSliderMaxValue.Value = Properties.Settings.Default.ScaleSliderMax;
            Scale.Maximum = Properties.Settings.Default.ScaleSliderMax;
            Scale.UpdateLayout();
            Threads.Maximum = Environment.ProcessorCount;
            Threads.Value = Properties.Settings.Default.Threads;
            Threads.UpdateLayout();
            CudaEnabled.IsChecked = Properties.Settings.Default.CudaEnabled;
        }

        private void OpenSettings(object sender, RoutedEventArgs e) => SettingsFlyout.IsOpen = true;

        private void OpenAbout(object sender, RoutedEventArgs e) => AboutFlyout.IsOpen = true;

        private void ApplyTheme()
        {
            if (!loading)
            {
                _ = ThemeManager.Current.ChangeTheme(this, $"{((bool)ThemeDark.IsChecked ? "Dark" : "Light")}.{ThemeAccent.SelectedValue}");
            }
        }

        private void ThemeDark_Checked(object sender, RoutedEventArgs e) => ApplyTheme();

        private void ThemeDark_Unchecked(object sender, RoutedEventArgs e) => ApplyTheme();

        private void ThemeAccent_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e) => ApplyTheme();

        private void SettingsFlyout_ClosingFinished(object sender, RoutedEventArgs e) => LoadSettings();

        private void SaveSettings_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Theme = $"{((bool)ThemeDark.IsChecked ? "Dark" : "Light")}.{ThemeAccent.SelectedValue}";
            Properties.Settings.Default.ScaleSliderMax = (int)ScaleSliderMaxValue.Value;
            Properties.Settings.Default.Threads = (int)Threads.Value;
            Properties.Settings.Default.CudaEnabled = (bool)CudaEnabled.IsChecked;
            Properties.Settings.Default.Save();
            SettingsFlyout.IsOpen = false;
            Scale.Maximum = (double)ScaleSliderMaxValue.Value;
            Scale.UpdateLayout();
        }

        private void BrowseOutput_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog openFolderDialog = new()
            {
                Description = "Select folder to save finished videos in",
                UseDescriptionForTitle = true,
            };
            if (openFolderDialog.ShowDialog() == true)
            {
                OutputDirectoryBox.Text = openFolderDialog.SelectedPath;
            }
        }

        private void VideoPath_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (File.Exists(InputVideo.Text))
            {
                InputPreview.Source = new Uri(InputVideo.Text, UriKind.Absolute);
            }
            else
            {
                inputVideoPaused = true;
                InputPreviewPlayPauseButton.IsEnabled = false;
                ProcessButton.IsEnabled = false;
                ProcessingTab.IsEnabled = false;
                OutputTab.IsEnabled = false;
                InputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
            }
        }

        private void InputVideoBrowse_Click(object sender, RoutedEventArgs e)
        {
            string acceptedFiles = string.Join(';', VideoFormats.Select(format => '*' + format.ToUpperInvariant()));
            VistaOpenFileDialog openFileDialog = new()
            {
                Filter = $"Accepted files ({acceptedFiles})|{acceptedFiles}",
                Title = "Select input video"
            };
            if (openFileDialog.ShowDialog() == true)
            {
                InputPreview.Stop();
                inputVideoPaused = true;
                InputPreviewPlayPauseButton.IsEnabled = true;
                ProcessButton.IsEnabled = true;
                InputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
                InputVideo.Text = openFileDialog.FileName;
            }
        }

        private void InputPreviewPlayPauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (InputPreview.Source != null)
            {
                if (inputVideoPaused)
                {
                    InputPreview.Play();
                    inputVideoPaused = false;
                    InputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPause;
                }
                else
                {
                    InputPreview.Pause();
                    inputVideoPaused = true;
                    InputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
                }
            }
        }

        private void ProcessButton_Click(object sender, RoutedEventArgs e)
        {
            AllowDrop = false;
            ProcessingTab.IsEnabled = true;
            MainTabControl.SelectedIndex = 1;
            InputTab.IsEnabled = false;
            float videoFramerate = Utils.GetVideoFramerate(InputVideo.Text);
            cas = new(InputVideo.Text, Properties.Settings.Default.OutputDirectory, WorkingDirectory, videoFramerate)
            {
                OutputFileName = $"{DateTimeOffset.Now.ToUnixTimeSeconds()}.mp4",
            };
            if (Properties.Settings.Default.CudaEnabled)
            {
                cas.CudaProfile.DecodingEnabled = true;
                cas.CudaProfile.EncodingEnabled = true;
            }
            cas.FFmpegDirectory = Environment.CurrentDirectory;
            cas.Threads = (int)Threads.Value;
            int scale = (int)Scale.Value;
            processingTask = Task.Run(() =>
            {
                Stopwatch timer = new();
                timer.Start();
                cas = cas.ExtractAudio();
                Dispatcher.Invoke(() =>
                {
                    ExtractFramesTab.IsEnabled = true;
                    ProcessingTabControl.SelectedIndex = 1;
                    ExtractAudioTab.IsEnabled = false;
                });
                if (processingCancellationToken.IsCancellationRequested)
                {
                    return;
                }
                cas = cas.ExtractFrames();
                Dispatcher.Invoke(() =>
                {
                    ApplyCasTab.IsEnabled = true;
                    ProcessingTabControl.SelectedIndex = 2;
                    ExtractFramesTab.IsEnabled = false;
                    TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Normal;
                });
                if (processingCancellationToken.IsCancellationRequested)
                {
                    return;
                }
                Task<ContentAwareScaleVideo> casTask = cas.PerformVideoAsync(ref processingCancellationToken, new Progress<PerformProgressReport>(i =>
                {
                    if (processingCancellationToken.IsCancellationRequested)
                    {
                        return;
                    }
                    Dispatcher.Invoke(() =>
                    {
                        FrameIndicator.Content = $"Frame {i.CurrentFrame}/{i.NumberOfFrames} ({Math.Round(i.FramesPerSecond, 2)} fps)";
                        TimeIndicator.Content = $"{i.EstimatedTimeUntilCompletion:hh\\:mm\\:ss} h:m:s estimated until completion";
                        FrameProgress.Value = i.Percent;
                        TaskbarItemInfo.ProgressValue = i.Percent / 100;
                    });
                }), scale);
                casTask.Wait(processingCancellationToken);
                cas = casTask.Result;
                Dispatcher.Invoke(() =>
                {
                    ApplyCasAudioTab.IsEnabled = true;
                    ProcessingTabControl.SelectedIndex = 3;
                    ApplyCasTab.IsEnabled = false;
                });
                if (processingCancellationToken.IsCancellationRequested)
                {
                    return;
                }
                cas = cas.PerformAudio();
                Dispatcher.Invoke(() =>
                {
                    RecombineTab.IsEnabled = true;
                    ProcessingTabControl.SelectedIndex = 4;
                    ApplyCasAudioTab.IsEnabled = false;
                });
                if (processingCancellationToken.IsCancellationRequested)
                {
                    return;
                }
                cas = cas.Recombine("Generated by CASVideo-GUI by Taconator");
                Dispatcher.Invoke(() =>
                {
                    OutputTab.IsEnabled = true;
                    OutputPreview.Source = new Uri(cas.OutputFilePath, UriKind.Absolute);
                    MainTabControl.SelectedIndex = 2;
                    RecombineTab.IsEnabled = false;
                    ProcessingTab.IsEnabled = false;
                    timer.Stop();
                    TimeTaken.Content = $"{timer.Elapsed:hh\\:mm\\:ss} hh:mm:ss taken";
                    TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.None;
                    ProcessingTabControl.SelectedIndex = 0;
                    AllowDrop = true;
                    FrameProgress.Value = 0;
                });
                cas.Dispose();
            }, processingCancellationToken);
        }

        private void StartOverButton_Click(object sender, RoutedEventArgs e)
        {
            OutputPreview.Source = null;
            InputPreview.Source = null;
            InputTab.IsEnabled = true;
            MainTabControl.SelectedIndex = 0;
            OutputTab.IsEnabled = false;
            InputVideo.Text = "";
            AllowDrop = true;
        }

        private void OutputPreviewPlayPauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (outputVideoPaused)
            {
                OutputPreview.Play();
                outputVideoPaused = false;
                OutputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPause;
            }
            else
            {
                OutputPreview.Pause();
                outputVideoPaused = true;
                OutputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
            }
        }

        private void InputPreview_MediaEnded(object sender, RoutedEventArgs e)
        {
            InputPreview.Stop();
            inputVideoPaused = true;
            InputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
        }

        private void OutputPreview_MediaEnded(object sender, RoutedEventArgs e)
        {
            OutputPreview.Stop();
            outputVideoPaused = true;
            OutputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
        }

        private void OpenInExplorerButton_Click(object sender, RoutedEventArgs e)
        {
            string path = Path.GetFullPath(OutputPreview.Source.AbsolutePath);
            string arguments = $"/select,\"{path}\"";
            _ = Process.Start("explorer.exe", arguments);
        }

        private void Scale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ScaleIndicator.Content = $"Scale ({Scale.Value}) [Default 2]";
        }

        private void Threads_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ThreadsIndicator.Text = $"Threads ({Threads.Value} / {Environment.ProcessorCount} available)";
        }

        private void MetroWindow_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                if (MainTabControl.SelectedIndex is 0 or 2)
                {
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    string filePath = files[0];
                    string filePathLower = filePath.ToLowerInvariant();
                    if (VideoFormats.Any(filetype => filePathLower.EndsWith(filetype)))
                    {
                        InputVideo.Text = files[0];
                        InputPreview.Stop();
                        inputVideoPaused = true;
                        InputPreviewPlayPauseButton.IsEnabled = true;
                        ProcessButton.IsEnabled = true;
                        OutputPreview.Source = null;
                        InputTab.IsEnabled = true;
                        MainTabControl.SelectedIndex = 0;
                        OutputTab.IsEnabled = false;
                        InputPreviewPlayPauseButtonIcon.Kind = MahApps.Metro.IconPacks.PackIconModernKind.ControlPlay;
                    }
                }
            }
        }

        private void MetroWindow_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                if (MainTabControl.SelectedIndex is 0 or 2)
                {
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    string filePathLower = files[0].ToLowerInvariant();
                    e.Effects = VideoFormats.Any(filetype => filePathLower.EndsWith(filetype)) ? DragDropEffects.Copy : DragDropEffects.None;
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void License_Click(object sender, RoutedEventArgs e)
        {
            _ = Process.Start("notepad.exe", "LICENSE");
        }

        private void MagickNet_Click(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo startInfo = new()
            {
                FileName = "https://github.com/dlemstra/Magick.NET",
                UseShellExecute = true,
            };
            _ = Process.Start(startInfo);
        }

        private void ImageMagick_Click(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo startInfo = new()
            {
                FileName = "https://imagemagick.org/index.php",
                UseShellExecute = true,
            };
            _ = Process.Start(startInfo);
        }

        private void Ffmpeg_Click(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo startInfo = new()
            {
                FileName = "http://ffmpeg.org/",
                UseShellExecute = true,
            };
            _ = Process.Start(startInfo);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure that you want to cancel", "Confirm", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                cancellationTokenSource.Cancel();
                while (!processingTask.IsCompleted)
                {
                    _ = Task.Delay(10);
                }
                Directory.Delete(WorkingDirectory, true);
                TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.None;
                cancellationTokenSource = new();
                processingCancellationToken = cancellationTokenSource.Token;
                MainTabControl.SelectedIndex = 0;
                ProcessingTabControl.SelectedIndex = 0;
                ProcessingTabControl.UpdateLayout();
                foreach (MetroTabItem tab in ProcessingTabControl.Items)
                {
                    tab.IsEnabled = false;
                }
                ProcessingTab.IsEnabled = false;
                AllowDrop = true;
            }
        }
    }
}
