# CASVideo-gui
GUI wrapper for my library for applying content aware scale to entire videos

## Requirements
- 64-bit processor and operating system
- Windows as your operating system

## Supported Formats
### For processing
- Audio Video Interleave (.AVI)
- Flash Video (.FLV)
- Matroska (.MKV)
- QuickTime (.MOV)
- MPEG-4 Part 14 (.MP4)
- WebM (.WEBM)
- Windows Media Video (.WMV)
### For preview
- Audio Video Interleave (.AVI)
- QuickTime (.MOV) [can be unpredictable sometimes]
- MPEG-4 Part 14 (.MP4)
- Windows Media Video (.WMV)
